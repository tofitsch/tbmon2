/* 
 * File:   SumToT.cc
 * Author: daniel
 * 
 * Created on 24. Februar 2014, 19:37
 */

#include "SumToT.h"

void SumToT::init(const TBCore* core)
{
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;
		double pitchX = dut->getDutPitchX();
		double pitchY = dut->getDutPitchY();
		int nCols = dut->getNcols();
		int nRows = dut->getNrows();
		int maxToT = dut->getMaxTot();

		SumToT::h_fullChipTot[iden] = new TH1D("", ";Pixel hit charge [ToT]", maxToT, -.5, maxToT-.5);
		SumToT::h_fullChipQ[iden] = new TH1D("", ";Pixel hit charge [e^{-}]", 150, 0, 100000);
		SumToT::h_maxClusterTot[iden] = new TH1D("", ";Max cluster charge [ToT]", maxToT, -.5, maxToT-.5);
		SumToT::h_maxClusterQ[iden] = new TH1D("", ";Max cluster charge [e^{-}]", 150, 0, 100000);
		SumToT::h_maxCellTot[iden] = new TH1D("", ";Max cell charge [ToT]", maxToT, -.5, maxToT-.5);
		SumToT::h_maxCellQ[iden] = new TH1D("", ";Max cell charge [e^{-}]", 150, 0, 100000);

		SumToT::h_matchClusterTot[iden] = new TH1D("", ";Matching cluster charge [ToT]", maxToT, 0, maxToT);
		SumToT::h_matchClusterQ[iden] = new TH1D("", ";Matching cluster charge [e^{-}]", 300, 0, 100000);
		SumToT::h_matchClusterQ_1[iden] = new TH1D("", ";Cluster charge [e^{-}]", 300, 0, 100000);
		SumToT::h_matchClusterQ_2[iden] = new TH1D("", ";Cluster charge [e^{-}]", 300, 0, 100000);
		SumToT::h_matchClusterQ_3[iden] = new TH1D("", ";Cluster charge [e^{-}]", 300, 0, 100000);
		SumToT::h_matchClusterQ_4[iden] = new TH1D("", ";Cluster charge [e^{-}]", 300, 0, 100000);
		SumToT::h_matchClusterQ_5plus[iden] = new TH1D("", ";Cluster charge [e^{-}]", 300, 0, 100000);

		//SumToT::h_matchClusterTotMap[iden] = new TH3D("", ";Column;Row;Cluster charge [ToT]", nCols, 0, pitchX, nRows, 0, pitchY, maxToT, 0, maxToT);
		//SumToT::h_matchClusterQMap[iden] = new TH3D("", ";Column;Row;Cluster charge [e^{-}]", nCols, 0, pitchX, nRows, 0, pitchY, 150, 0, 100000);
		
		SumToT::h_matchClusterTotMap[iden] = new TH3D("", ";Column;Row;Cluster charge [ToT]", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5, maxToT, 0, maxToT);
		SumToT::h_matchClusterQMap[iden] = new TH3D("", ";Column;Row;Cluster charge [e^{-}]", nCols, -.5, nCols-.5, nRows, -.5, nRows-.5, 150, 0, 100000);

		for(auto geometry: dut->pixelGeometries)
		{
			double maxPixelPitchX = geometry->getMaxPitchX();
			double maxPixelPitchY = geometry->getMaxPitchY();
			
			TH2D* elecMap = new TH2D("", ";X-Distance to electrode [#mum];Y-Distance to electrode [#mum]", 50, -100, 100, pitchY, -pitchY, pitchY);
			// pixel pitch X wird nicht verwendet, 
			TH3D* elecMapTot = new TH3D("", ";X-Distance to electrode [#mum];Y-Distance to electrode [#mum];Matching cluster charge [ToT]", 
													50, -100., 100., 
													maxPixelPitchY, -maxPixelPitchY, maxPixelPitchY, 
													0.5 * maxToT, 0, maxToT);
			TH1D* elecDist = new TH1D("", ";Distance to center of electrode [#mum]", 50, 0, 100);
			TH2D* elecDistTot = new TH2D("", ";Distance to center of electrode [#mum];Matching cluster charge [ToT]", 50, 0, 100., 0.5 * maxToT, 0, maxToT);
			
			SumToT::h_elecMap[iden].push_back(elecMap);
			SumToT::h_elecMapTot[iden].push_back(elecMapTot);
			SumToT::h_elecDist[iden].push_back(elecDist);
			SumToT::h_elecDistTot[iden].push_back(elecDistTot);
		}

		SumToT::doCharge = false;
	}
}

void SumToT::event(const TBCore* core, const TBEvent* event)
{
	DUT* dut = event->dut;
	int iden = event->iden; 

	// Require that we have hits
	if(event->fHits != kGood)
	{
		return;
	}
	
	// Check that clusters have been made successfully
	if(event->fClusters != kGood)
	{
		return;
	}
	
	// Check that track has passed all cuts
	if(event->fTracks != kGood)
	{
		return;
	}
	
	int sumHitToT = 0;
	for(auto hit: event->hits)
	{
		sumHitToT += hit->tot;
	}
	SumToT::h_fullChipTot[iden]->Fill(sumHitToT);
	
	if(SumToT::doCharge == true)
	{
		double sumHitCharge = 0.0;
		for(auto hit: event->hits)
		{
			sumHitCharge += (!dut->totcalib->hasCalib()) ? hit->tot : event->dut->q(hit->tot, hit->col, hit->row);
		}
		SumToT::h_fullChipQ[iden]->Fill((int) sumHitCharge);
	}
	
	TBCluster* maxToTCluster = TBCluster::getMaxToTCluster(&(event->clusters));
	int sumClusterToT = TBCluster::getSumToT(maxToTCluster);
	if(maxToTCluster != NULL and sumClusterToT > 0)
	{
		if(maxToTCluster->fClusterGood == kGood)
		{
			SumToT::h_maxClusterTot[iden]->Fill(sumClusterToT);
			if(SumToT::doCharge == true)
			{
				SumToT::h_maxClusterQ[iden]->Fill(TBCluster::getSumCharge(maxToTCluster, event));
			}
		}
	}
	
	
	for(auto tbtrack: event->tracks)
	{
		// Use only clusters on tracks in the central region and exclude bad regions
		if(tbtrack->fTrackMaskedRegion == kGood)
		{
			continue;
		}
		
		if(tbtrack->fMatchedCluster != kGood)
		{
			continue;
		}
		
		if(tbtrack->matchedCluster->fCentralPixelCluster != kGood)
		{
			continue;
		}
		
		int matchedClusterSumToT = TBCluster::getSumToT(tbtrack->matchedCluster);
		int matchedClusterSumCharge = TBCluster::getSumCharge(tbtrack->matchedCluster, event);
		if(matchedClusterSumToT <= 0)
		{
			continue;
		}
		
		SumToT::h_matchClusterTot[iden]->Fill(matchedClusterSumToT);
		
		if(SumToT::doCharge == true)
		{
			SumToT::h_matchClusterQ[iden]->Fill(matchedClusterSumCharge);
		}
		
		double chargeWeightedX = TBCluster::getChargeWeightedX(tbtrack->matchedCluster, event);
		double chargeWeightedY = TBCluster::getChargeWeightedY(tbtrack->matchedCluster, event);
		int chargeCol;
		int chargeRow;
		
		dut->getColRow(chargeWeightedX, chargeWeightedY, &chargeCol, &chargeRow);
		
		SumToT::h_matchClusterTotMap[iden]->Fill(chargeCol, chargeRow, matchedClusterSumToT);
		//SumToT::h_matchClusterTotMap[iden]->Fill(chargeWeightedX, chargeWeightedY, matchedClusterSumToT);
	
		if(SumToT::doCharge == true) 
		{
			SumToT::h_matchClusterQMap[iden]->Fill(chargeCol, chargeRow, matchedClusterSumCharge);
			//SumToT::h_matchClusterQMap[iden]->Fill(chargeWeightedX, chargeWeightedY, matchedClusterSumCharge);
		}
		
		switch(tbtrack->matchedCluster->hits.size())
		{
			case 1:
				if(SumToT::doCharge == true)
				{
					SumToT::h_matchClusterQ_1[iden]->Fill(matchedClusterSumCharge);
				}
				break;
			case 2:
				if(SumToT::doCharge == true)
				{
					SumToT::h_matchClusterQ_2[iden]->Fill(matchedClusterSumCharge);
				}
				break;
			case 3:
				if(SumToT::doCharge == true)
				{
					SumToT::h_matchClusterQ_3[iden]->Fill(matchedClusterSumCharge);
				}
				break;
			case 4:
				if(SumToT::doCharge == true)
				{
					SumToT::h_matchClusterQ_4[iden]->Fill(matchedClusterSumCharge);
				}
				break;
			default:
				if(SumToT::doCharge == true) 
				{
					SumToT::h_matchClusterQ_5plus[iden]->Fill(matchedClusterSumCharge);
				}
				break;
		}
		
		if(dut->getNumElec() == 0)
		{
			continue;
		}
		
		int trackCol = tbtrack->trackCol;
		int trackRow = tbtrack->trackRow;
		
		PixelGeometry* pixGeo = dut->pixelArray[trackCol][trackRow].getGeometry();
		int geometryNum = dut->getGeometryNumber(pixGeo);
		double maxPixelPitchX = pixGeo->getMaxPitchX();
		double maxPixelPitchY = pixGeo->getMaxPitchY();
		
		double elecDistX;
		double elecDistY;
		
		dut->getElectrodeDistance(trackCol, trackRow, tbtrack->trackX, tbtrack->trackY, &elecDistX, &elecDistY);

		// track distance to electrode
		double elecDist = std::sqrt(elecDistX*elecDistX+elecDistY*elecDistY);
	
		SumToT::h_elecMap[iden][geometryNum]->Fill(elecDistX, elecDistY);
		SumToT::h_elecMapTot[iden][geometryNum]->Fill(elecDistX, elecDistY, matchedClusterSumToT);
		SumToT::h_elecDist[iden][geometryNum]->Fill(elecDist);
		SumToT::h_elecDistTot[iden][geometryNum]->Fill(elecDist, matchedClusterSumToT);
		
	}
}

void SumToT::finalize(const TBCore* core)
{
	core->output->processName = SumToT::name;
	
	char* histoTitle = new char[500];
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;

		// set up cuts
		core->output->cuts = "good tracks with matched cluster";
		core->output->currentPreprocessCuts = "";
		// -----------
		
		std::sprintf(histoTitle, "Full Chip ToT DUT %i", iden);
		SumToT::h_fullChipTot[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "fullChipToT_dut_%i", iden);
		SumToT::h_fullChipTot[iden]->SetName(histoTitle);
		core->output->drawAndSave(SumToT::h_fullChipTot[iden], "", "em");
		
		if(SumToT::doCharge == true)
		{
			std::sprintf(histoTitle, "Full Chip Charge DUT %i", iden);
			SumToT::h_fullChipQ[iden]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "fullChipQ_dut_%i", iden);
			SumToT::h_fullChipQ[iden]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_fullChipQ[iden], "", "em");
		}
		
		/*
		std::sprintf(histoTitle, "Max Cell ToT DUT %i", iden);
		SumToT::h_maxCellTot[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "maxCellToT_dut_%i", iden);
		SumToT::h_maxCellTot[iden]->SetName(histoTitle);
		core->output->drawAndSave(SumToT::h_maxCellTot[iden], "", "em");
		*/
		
		if(SumToT::doCharge == true)
		{
			std::sprintf(histoTitle, "Max Cell Charge DUT %i", iden);
			SumToT::h_maxCellQ[iden]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "maxCellQ_dut_%i", iden);
			SumToT::h_maxCellQ[iden]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_maxCellQ[iden], "", "em");
		}
		
		std::sprintf(histoTitle, "Max CLuster ToT DUT %i", iden);
		SumToT::h_maxClusterTot[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "maxClusterToT_dut_%i", iden);
		SumToT::h_maxClusterTot[iden]->SetName(histoTitle);
		core->output->drawAndSave(SumToT::h_maxClusterTot[iden], "", "em");
		
		if(SumToT::doCharge == true)
		{
			std::sprintf(histoTitle, "Max Cluster Charge DUT %i", iden);
			SumToT::h_maxClusterQ[iden]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "maxClusterQ_dut_%i", iden);
			SumToT::h_maxClusterQ[iden]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_maxClusterQ[iden], "", "em");
		}
		
		std::sprintf(histoTitle, "Matched Cluster ToT DUT %i", iden);
		SumToT::h_matchClusterTot[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "matchClusterToT_dut_%i", iden);
		SumToT::h_matchClusterTot[iden]->SetName(histoTitle);
		core->output->drawAndSave(SumToT::h_matchClusterTot[iden], "", "em");
		
		if(SumToT::doCharge == true)
		{
			std::sprintf(histoTitle, "Matched Cluster Charge DUT %i", iden);
			SumToT::h_matchClusterQ[iden]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "matchClusterQ_dut_%i", iden);
			SumToT::h_matchClusterQ[iden]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_matchClusterQ[iden], "", "em");

			std::sprintf(histoTitle, "Matched Cluster Size 1 Charge DUT %i", iden);
			SumToT::h_matchClusterQ_1[iden]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "matchClusterQ_1_dut_%i", iden);
			SumToT::h_matchClusterQ_1[iden]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_matchClusterQ_1[iden], "", "em");

			std::sprintf(histoTitle, "Matched Cluster Size 2 Charge DUT %i", iden);
			SumToT::h_matchClusterQ_2[iden]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "matchClusterQ_2_dut_%i", iden);
			SumToT::h_matchClusterQ_2[iden]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_matchClusterQ_2[iden], "", "em");

			std::sprintf(histoTitle, "Matched Cluster Size 3 Charge DUT %i", iden);
			SumToT::h_matchClusterQ_3[iden]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "matchClusterQ_3_dut_%i", iden);
			SumToT::h_matchClusterQ_3[iden]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_matchClusterQ_3[iden], "", "em");

			std::sprintf(histoTitle, "Matched Cluster Size 4 Charge DUT %i", iden);
			SumToT::h_matchClusterQ_4[iden]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "matchClusterQ_4_dut_%i", iden);
			SumToT::h_matchClusterQ_4[iden]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_matchClusterQ_4[iden], "", "em");

			std::sprintf(histoTitle, "Matched Cluster Size 5+ Charge DUT %i", iden);
			SumToT::h_matchClusterQ_5plus[iden]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "matchClusterQ_5plus_dut_%i", iden);
			SumToT::h_matchClusterQ_5plus[iden]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_matchClusterQ_5plus[iden], "", "em");
		}
		
		std::sprintf(histoTitle, "Matched Cluster ToT Map DUT %i", iden);
		SumToT::h_matchClusterTotMap[iden]->SetTitle(histoTitle);
		std::sprintf(histoTitle, "matchClusterToTMap_dut_%i", iden);
		SumToT::h_matchClusterTotMap[iden]->SetName(histoTitle);
		core->output->drawAndSave(SumToT::h_matchClusterTotMap[iden], "", "e");
		
		TProfile2D* totmap2d = SumToT::h_matchClusterTotMap[iden]->Project3DProfile("yx");
		totmap2d->SetTitle(";Column;Row");
		std::sprintf(histoTitle, "Matched Cluster ToT Map DUT %i", iden);
		totmap2d->SetTitle(histoTitle);
		std::sprintf(histoTitle, "ToTMap_dut_%i", iden);
		totmap2d->SetName(histoTitle);
		core->output->drawAndSave(totmap2d, "colz", "e");
		delete totmap2d;
		
		for(auto geometry: dut->pixelGeometries)
		{
			int geometryNumber = dut->getGeometryNumber(geometry);
			
			std::string edge = "";
			
			if(geometry->isEdgePixel() == true)
			{
				edge = "(Edge Pixel)";
			}
			
			std::sprintf(histoTitle, "Elec Map DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			SumToT::h_elecMap[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "elecMap_dut_%i_geometry_%i", iden, geometryNumber);
			SumToT::h_elecMap[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_elecMap[iden][geometryNumber], "", "e");
			
			std::sprintf(histoTitle, "Elec Map ToT DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			SumToT::h_elecMapTot[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "elecMapToT_dut_%i_geometry_%i", iden, geometryNumber);
			SumToT::h_elecMapTot[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_elecMapTot[iden][geometryNumber], "", "e");
			
			std::sprintf(histoTitle, "Elec Distance DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			SumToT::h_elecDist[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "elecDist_dut_%i_geometry_%i", iden, geometryNumber);
			SumToT::h_elecDist[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_elecDist[iden][geometryNumber], "", "em");
			
			std::sprintf(histoTitle, "Elec Distance ToT DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			SumToT::h_elecDistTot[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "elecDistToT_dut_%i_geometry_%i", iden, geometryNumber);
			SumToT::h_elecDistTot[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(SumToT::h_elecDistTot[iden][geometryNumber], "colz", "e");
			
			TProfile2D* totmap2d = SumToT::h_elecMapTot[iden][geometryNumber]->Project3DProfile("yx");
			totmap2d->SetTitle(";Distance from read-out electrode center [#mum];Distance from read-out electrode center [#mum];Matching Cluster ToT");
			std::sprintf(histoTitle, "Elec Map ToT DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			totmap2d->SetTitle(histoTitle);
			std::sprintf(histoTitle, "elecMapToT_2D_dut_%i_geometry_%i", iden, geometryNumber);
			totmap2d->SetName(histoTitle);
			core->output->drawAndSave(totmap2d, "colz", "e");
			delete totmap2d;
			
			char* title = new char[500];
			std::sprintf(title, "Elec Distance ToT DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			std::sprintf(histoTitle, "elecDistToT_dut_%i_geometry_%i", iden, geometryNumber);
			
			
			SumToT::ElecDistToT(core, title, histoTitle ,SumToT::h_elecDistTot[iden][geometryNumber]);
			delete [] title;
			
			delete SumToT::h_elecMap[iden][geometryNumber];
			delete SumToT::h_elecMapTot[iden][geometryNumber];
			delete SumToT::h_elecDist[iden][geometryNumber];
			delete SumToT::h_elecDistTot[iden][geometryNumber];
		}
		
		SumToT::h_elecMap[iden].clear();
		SumToT::h_elecMapTot[iden].clear();
		SumToT::h_elecDist[iden].clear();
		SumToT::h_elecDistTot[iden].clear();
		delete SumToT::h_fullChipTot[iden];
		delete SumToT::h_fullChipQ[iden];
		delete SumToT::h_maxClusterTot[iden];
		delete SumToT::h_maxClusterQ[iden];
		delete SumToT::h_maxCellTot[iden];
		delete SumToT::h_maxCellQ[iden];
		delete SumToT::h_matchClusterTot[iden];
		delete SumToT::h_matchClusterQ[iden];
		delete SumToT::h_matchClusterQ_1[iden];
		delete SumToT::h_matchClusterQ_2[iden];
		delete SumToT::h_matchClusterQ_3[iden];
		delete SumToT::h_matchClusterQ_4[iden];
		delete SumToT::h_matchClusterQ_5plus[iden];
		delete SumToT::h_clusterq_no_bg[iden];

		delete SumToT::h_matchClusterTotMap[iden];
		delete SumToT::h_matchClusterQMap[iden];
	}
	SumToT::h_fullChipTot.clear();
	SumToT::h_fullChipQ.clear();
	SumToT::h_maxClusterTot.clear();
	SumToT::h_maxClusterQ.clear();
	SumToT::h_maxCellTot.clear();
	SumToT::h_maxCellQ.clear();
	SumToT::h_matchClusterTot.clear();
	SumToT::h_matchClusterQ.clear();
	SumToT::h_matchClusterQ_1.clear();
	SumToT::h_matchClusterQ_2.clear();
	SumToT::h_matchClusterQ_3.clear();
	SumToT::h_matchClusterQ_4.clear();
	SumToT::h_matchClusterQ_5plus.clear();
	SumToT::h_clusterq_no_bg.clear();

	SumToT::h_matchClusterTotMap.clear();
	SumToT::h_matchClusterQMap.clear();
	
	SumToT::h_elecMap.clear();
	SumToT::h_elecMapTot.clear();
	SumToT::h_elecDist.clear();
	SumToT::h_elecDistTot.clear();
	delete [] histoTitle;
}

void SumToT::ElecDistToT(const TBCore* core, const char* histoTitle, const char* histoName, TH2D* elecDistTot)
{
	//project each bin
	elecDistTot->Rebin2D(2, 1);
	int nBins = elecDistTot->GetNbinsX();

	// Define two distances that should be compared directly
	double d1 = 20.0; // outside electrode!
	double d2 = 4.0; // inside electrode!
	TString d1s = "";
	TString d2s = "";
	TH1D* h1 = NULL;
	TH1D* h2 = NULL;

	for(int bin = 1; bin != nBins; bin++)
	{
		//TH1D* h = (TH1D*) elecDistTot->ProjectionY(TString::Format("pr%i", bin), bin, bin, "e");
		TH1D* h = (TH1D*) elecDistTot->ProjectionY(TString::Format("pr%i", bin), bin, bin);
		h->SetStats(false);
		h->SetTitle(";Cluster charge [ToT];Tracks");
		h->SetTitle(histoTitle);
		h->SetFillStyle(1001);
		h->SetFillColor(kRed - 4);
		TH1D* h_copy = (TH1D*) h->DrawCopy("hist");

		double xMin = 0.0;
		double xMax = 0.0;
		xMin = h_copy->GetXaxis()->GetBinLowEdge(bin);
		xMax = h_copy->GetXaxis()->GetBinUpEdge(bin);
		char* text = new char[500];
		std::sprintf(text, "[%.1f,%.1f] #mum", xMin, xMax);

		TString fileName = TString::Format("%s_%i", histoName, bin);
		h->SetName(fileName);
	
		core->output->drawAndSave(h, "", "em", text);

		if(d1 > xMin and d1 <= xMax)
		{
			h1 = h;
			d1s = TString::Format("[%.1f,%.1f] #mum", xMin, xMax);
		} else if(d2 > xMin and d2 <= xMax)
		{
			h2 = h;
			d2s = TString::Format("[%.1f,%.1f] #mum", xMin, xMax);
		}
		//delete h;
		delete h_copy;
		delete [] text;
	}
	
	if(h1 != NULL and h2 != NULL)
	{
		h2->SetTitle(";Arbitrary Units;Cluster charge [ToT]");
		h2->SetTitle(histoTitle);
		h2->SetFillColor(kBlack);
		h2->SetFillStyle(3004);
	
		h1->SetFillColor(kRed - 4);
		h1->SetLineColor(kRed);
		h1->SetFillStyle(1001);

		/*
		TLegend* leg = new TLegend(0.65, 0.7, 0.88, 0.9);
		leg->SetFillColor(0);
		leg->SetBorderSize(0);
		leg->AddEntry(h1, d1s, "F");
		leg->AddEntry(h2, d2s, "F");
		h2->Add
		*/
		TString fileName = TString::Format("%s_Comp", histoName);
		
		std::vector<TBOutputTNamed*> hists = std::vector<TBOutputTNamed*>();
		hists.push_back(new TBOutputTNamed(h1, "histo", "em", false));
		hists.push_back(new TBOutputTNamed(h2, "histo sames", "em", false));

		char* text = new char[500];
		std::sprintf(text, "red: %s; black: %s", d1s.Data(), d2s.Data());
		
		core->output->drawAndSave(hists, fileName.Data(), text);
		
		//delete leg;
		delete [] text;
		delete h1;
	}
}