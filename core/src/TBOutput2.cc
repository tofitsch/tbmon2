/* 
 * File:   TBOutput2.cc
 * Author: daniel
 * 
 * Created on 10. März 2014, 20:19
 */

#include "TBOutput2.h"
#include <TMacro.h>
#include <TSystemFile.h>

TBOutputTNamed::TBOutputTNamed(TNamed* hist, std::string drawOption, std::string statOption, bool writeHist)
{
	TBOutputTNamed::hist = hist;
	TBOutputTNamed::drawOption = drawOption;
	TBOutputTNamed::statOption = statOption;
	TBOutputTNamed::writeHist = writeHist;
}

TBOutputTNamed::~TBOutputTNamed()
{
	TBOutputTNamed::hist = NULL;
	TBOutputTNamed::drawOption = "";
	TBOutputTNamed::statOption = "";
}

TBOutput2::TBOutput2(TBCore* core)
{
	TBOutput2::core = core;
	
	// create ROOT outputfile
	int firstRun = core->tbconfig->rawDataRuns.front();
	int lastRun = core->tbconfig->rawDataRuns.back();
	TBOutput2::preSet = (core->tbconfig->outputPath)+(core->tbconfig->rawDataName)+(std::to_string(firstRun)+"-"+std::to_string(lastRun));
	std::string rootFileName = TBOutput2::preSet+".root";
	
	// TODO implement overwriting flag
	
	TBOutput2::tOutFile = new TFile(rootFileName.c_str(), "RECREATE");
	TBOutput2::tOutFile->cd();
	TBOutput2::tCanvasDirName = "canvas";
	TBOutput2::processName = "";
	TBOutput2::cuts = "";
	TBOutput2::currentPreprocessCuts = "";
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;
		TBOutput2::preprocessCuts[iden] = "";
	}
}


TBOutput2::~TBOutput2()
{
	TBOutput2::core = NULL;
	
	TBOutput2::tOutFile->cd();
	TBOutput2::tOutFile->Close();
	delete TBOutput2::tOutFile;
	TBOutput2::tOutFile = NULL;
}

void TBOutput2::setUpOutputPath()
{
	TBOutput2::tOutFile->cd();

	if(!TBOutput2::tOutFile->GetDirectory(TBOutput2::processName.c_str()))
	{
		TBOutput2::tOutFile->mkdir(TBOutput2::processName.c_str());
	}
	TBOutput2::tOutFile->cd(TBOutput2::processName.c_str());
	
	if(!TBOutput2::tOutFile->GetDirectory(TBOutput2::tCanvasDirName.c_str()))
	{
		TBOutput2::tOutFile->mkdir((TBOutput2::processName+"/"+TBOutput2::tCanvasDirName).c_str());
	}
	
	TBOutput2::savePath = TBOutput2::core->tbconfig->outputPath+TBOutput2::core->tbconfig->plotPath;
	
	// TODO create preocess directory 
}

void TBOutput2::drawAndSave(TNamed* hist, std::string drawOption, std::string statOption, std::string histComment)
{
	std::vector<TBOutputTNamed*> hists = std::vector<TBOutputTNamed*>();
	hists.push_back(new TBOutputTNamed(hist, drawOption, statOption));
	
	std::string histName = std::string(hist->GetName());
	
	TBOutput2::drawAndSave(hists, histName, histComment);
}

void TBOutput2::drawAndSave(std::vector<TBOutputTNamed*> hists, std::string histName, std::string histComment)
{
	TCanvas* canv = TBOutput2::getTBOutputCanvas(histName.c_str());
	canv->cd();
	
	TPad* statPad = TBOutput2::getTBOutputCanvasStatPad(canv);
	TPad* histPad = TBOutput2::getTBOutputCanvasHistPad(canv);
	
	int optionStatSum = 0;
	for(auto tbtnamed: hists)
	{
		if(tbtnamed->statOption.compare("") != 0)
		{
			optionStatSum++;
		}
	}
	double optionStatScale = (optionStatSum != 0) ? 1/(double)optionStatSum : 1.0;
	
	int optionStatCount = 0;
	for(auto tbtnamed: hists)
	{
		TNamed* h = tbtnamed->hist;
		void* hh = h->IsA()->DynamicCast(h->Class(), h);
		TH1* hist = (TH1*) hh;
		
		if(tbtnamed->statOption.compare("") != 0)
		{
			hist->SetStats(kTRUE);
			gStyle->SetOptStat(tbtnamed->statOption.c_str());
		}
		else
		{
			hist->SetStats(kFALSE);
		}
		
		histPad->cd();
		hist->Draw(tbtnamed->drawOption.c_str());
		gPad->Update();
		
		if(tbtnamed->statOption.compare("") != 0)
		{
			TPaveStats* st;
			st = (TPaveStats*) hist->GetListOfFunctions()->FindObject("stats")->Clone("stats");
			st->SetX1NDC(0);
			st->SetY1NDC(optionStatScale*optionStatCount+0.01);
			optionStatCount++;
			st->SetX2NDC(0.98);
			st->SetY2NDC(optionStatScale*optionStatCount-0.01);
			st->SetTextColor(hist->GetLineColor());
			statPad->cd();
			st->Draw();
			// TODO
			((TH1*) canv->FindObject(hist->GetName()))->SetStats(kFALSE);
		}
		
		TBOutput2::setUpOutputPath();
		if(tbtnamed->writeHist)
		{
			hist->Write();
		}
		
		delete tbtnamed;
	}
	TBOutput2::setStandardTBOutputText(canv, histComment);
	
	canv->cd();
	TBOutput2::drawAndSaveCanvas(canv);
}



TCanvas* TBOutput2::getTBOutputCanvas(std::string canvasName)
{
	TCanvas* canv = new TCanvas(canvasName.c_str());
	canv->cd();
	
	TPad* pad1 = new TPad("textPad", "textPad", 0.05, 0, 0.74, 0.14);
	TPad* pad2 = new TPad("statPad", "statPad", 0.75, 0, 0.95, 0.14);
	TPad* pad3 = new TPad("histPad", "histPad", 0, 0.15, 1, 1);
	
	pad1->Draw();
	pad2->Draw();
	pad3->Draw();
	
	return canv;
}

TPad* TBOutput2::getTBOutputCanvasTextPad(TCanvas* canv)
{
	return (TPad*) canv->FindObject("textPad");
}

TPad* TBOutput2::getTBOutputCanvasStatPad(TCanvas* canv)
{
	return (TPad*) canv->FindObject("statPad");
}

TPad* TBOutput2::getTBOutputCanvasHistPad(TCanvas* canv)
{
	return (TPad*) canv->FindObject("histPad");
}

void TBOutput2::drawAndSaveCanvas(TCanvas* canv)
{
	TBOutput2::setUpOutputPath();
	TBOutput2::tOutFile->cd((TBOutput2::processName+"/"+TBOutput2::tCanvasDirName).c_str());
	
	// TODO save canvas to file
	if(false)
	{
		canv->SaveAs(canv->GetName());
	}
	
	canv->Write();
	
	delete canv;
}

void TBOutput2::setStandardTBOutputText(TCanvas* canv, std::string histComment)
{
	TPad* textPad = TBOutput2::getTBOutputCanvasTextPad(canv);
	textPad->cd();
	
	TPaveText* pt = new TPaveText(0, 0, 1, 1, "NDC");
	pt->SetBorderSize(0);
	pt->SetFillColor(kWhite);
	pt->AddText(histComment.c_str());
	pt->AddText(TBOutput2::cuts.c_str());
	pt->AddText(TBOutput2::currentPreprocessCuts.c_str());
	pt->Draw();
}

void TBOutput2::writeFilesIntoOutputFile(std::string path)
{
	TBOutput2::tOutFile->cd();
	
	TMacro* macro = new TMacro(path.c_str());
	macro->Write();
	delete macro;
	
}