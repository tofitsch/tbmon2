/* 
 * File:   TBDutConfig.h
 *
 * Created on 30. April 2013
 */
#ifndef TBDUTCONFIG_H
#define	TBDUTCONFIG_H

// default libs
#include <string>
#include <vector>

// rectangle config
class TBRectangleConfig
{
public:
	// lower left position of the angle
	double lowerLeftX;
	double lowerLeftY;
	// upper right position of the angle
	double upperRightX;
	double upperRightY;
};

// pixel config
class TBPixelConfig
{
public:
	// setup edge pixel
	bool edge;
	
	// position of the first pixel of this type
	int firstPixelCol;
	int firstPixelRow;
	double firstPixelX;
	double firstPixelY;
	// periode of this pixel
	int periodPixelCol;
	int periodPixelRow;
	double periodPixelX;
	double periodPixelY;
	// count of this pixel
	int countPixelCol;
	int countPixelRow;
	// period of mirroring
	int periodMirroringCol;
	// rectangle of the pixel
	std::vector<TBRectangleConfig> rectangle;
	// number of electrode
	int numElec;
	
};

// main DUT Config
class TBDutConfig
{
public:
	// DUT name
	std::string DUTName;
	// DUT real size
	double DUTsizeX;
	double DUTsizeY;
	double DUTthickness;
	// DUT iden
	int iden;
	// count of rows and colums
	int cols;
	int rows;
	// totcalib file path
	std::string totcalibPath;
	// lv1 set
	int lv1Min;
	int lv1Max;
	// mask malfuntioning pixels
	int maskNeighborsColRadius;
	int maskNeighborsRowRadius;

	bool maskDeadPixels;
	// z-position
	double zPos;
	// matching	
	double matchX; //wenn isMatch in der Clusterklasse fertig ist wieder entfernen
	double matchY; //wenn isMatch in der Clusterklasse fertig ist wieder entfernen
	
	double matchPixelMarginX;
	double matchPixelMarginY;
	
	// mask rows and colums before preprocessing
	std::vector<int> maskCol;
	std::vector<int> maskRow;
	// all pixel type
	std::vector<TBPixelConfig> pixel;
	// max Tot value
	int maxTot;
	
	// TEST delete later - number of electrode 
	int numElec;
};

#endif	/* TBDUTCONFIG_H */

