/* 
 * File:   TBOutput2.h
 * Author: daniel
 *
 * Created on 10. März 2014, 20:19
 */

#ifndef TBOUTPUT2_H
#define	TBOUTPUT2_H

#include <assert.h>
#include <stdio.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <vector>

#include <sys/stat.h>

#include <TFile.h>
#include <TCanvas.h>
#include <TNamed.h>
#include <TH1.h>
#include <TH1D.h>

class TBCore;
class TBOutput;
class TBOutput2;
#include "TBCore.h"

class TBOutputTNamed
{
public:
	TNamed* hist;
	std::string drawOption;
	std::string statOption;
	bool writeHist;
	
	TBOutputTNamed(TNamed* hist, std::string drawOption = "", std::string statOption = "", bool writeHist = true);
	~TBOutputTNamed();
};



class TBOutput2
{
private:
	TBCore* core;
	std::string tCanvasDirName;
	std::string savePath;
	
	std::string preSet;
	
	
	void setUpOutputPath();
	
public:
	TFile* tOutFile;
	
	std::string processName;
	std::string cuts;
	std::string currentPreprocessCuts;
	std::map<int, std::string> preprocessCuts;
	
	TBOutput2(TBCore* core);
	virtual ~TBOutput2();
	void drawAndSave(TNamed* hist, std::string drawOption = "", std::string statOption= "", std::string histComment = "");
	void drawAndSave(std::vector<TBOutputTNamed*> hists, std::string histName, std::string histComment = "");
	TCanvas* getTBOutputCanvas(std::string canvasName);
	TPad* getTBOutputCanvasTextPad(TCanvas* canv);
	TPad* getTBOutputCanvasStatPad(TCanvas* canv);
	TPad* getTBOutputCanvasHistPad(TCanvas* canv);
	void drawAndSaveCanvas(TCanvas* canv);
	void setStandardTBOutputText(TCanvas* canv, std::string histComment);
	void writeFilesIntoOutputFile(std::string path);
	
};

#endif	/* TBOUTPUT2_H */

