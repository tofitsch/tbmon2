/* 
 * File:   PixelMasker.h
 * Author: daniel
 *
 * Created on 13. März 2014, 15:31
 */

#ifndef PIXELMASKER_H
#define	PIXELMASKER_H

#include "TBPreprocess.h"

class PixelMasker : public TBPreprocess
{

 public:
  PixelMasker()
  {
    TBPreprocess::name = "PixelMasker";
  }
  
  virtual void init(TBCore* core);
};

// class factories
extern "C" TBPreprocess* create()
{
	return new PixelMasker;
}

extern "C" void destroy(TBPreprocess* tbp)
{
	delete tbp;
}

#endif	/* PIXELMASKER_H */

